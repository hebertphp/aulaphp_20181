create table usuario(
  id int not null primary key AUTO_INCREMENT,
  login varchar(50) not null,
  senha varchar(32) not null,
  data date not null,
  obs text
);